Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: afio
Upstream-Contact: Koen Holtman <k.holtman@chello.nl>
Source: https://github.com/kholtman/afio
X-Upstream-Vcs: git clone https://github.com/kholtman/afio.git
X-Upstream-Bugs: https://github.com/kholtman/afio/issues

Files: *
Copyright:
 1995-2016 Koen Holtman <koen@hep.caltech.edu>, <koen.holtman@ieee.org>, <k.holtman@chello.nl> (the current maintainer)
 1994 Anders Baekgaard <ab@osiris.cpk.auc.dk>
 1993-1994 Dave Gymer <dgymer@gdcarc.co.uk>
 1993 Andrew Stevens <as@prg.oxford.ac.uk>
 1985 Mark Brukhartz <..!ihnp4!laidbak!mdb>
 Jeff Buhrt <uunet!sawmill!prslnk!buhrt>
Comment:
 Original author of the program is Mark Brukhartz.
 Current maintainer is Koen Holtman.
 For years, see patchlevel.h and HISTORY file.
 Jeff Buhrt is mentioned briefly in afio.c and afio.1 but
 no year information.
License: Custom

Files: debian/*
Copyright: 1997-2004 Dirk Eddelbuettel <edd@debian.org>
           2005-2007, 2011 Erik Schanze <eriks@debian.org>, <schanzi_@gmx.de>
           2012-2016 Jari Aalto <jari.aalto@cante.net>
           2019      Giovani Augusto Ferreira <giovani@debian.org>
           2020- xiao sheng wen <atzlinux@sina.com>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in "/usr/share/common-licenses/GPL-2".

License: Custom
 The large beginning section of the main code file afio.c follows. It
 refers to the GNU GPL, GNU LGPL and Perl Artistic licenses.
 /*
  * afio.c
  *
  * Manipulate archives and files.
  *
  * This software was written by Mark Brukhartz at Lachman Associates,
  * Inc..  Additional code was written by a large cast of people.
  *
  * Licensing and (re)distribution
  * ------------------------------
  *
  * THE SUMMARY INFORMATION BELOW WAS WRITTEN FOR THE BENEFIT OF
  * SOFTWARE DISTRIBUTORS
  *
  * Because of historical reasons, different parts of this software
  * package are covered by different licenses.  However:
  *
  * A) This software package as a whole may be re-distributed by any
  *    method that satisfies the conditions of both the Perl "Artistic
  *    License" and the GNU Library General Public License.
  *
  * B) According to the theory.html file of the Sunsite Archive
  *    Maintainers, this implies that the correct LSM template field
  *    is:
  *
  *          Copying-policy: LGPL
  *
  * C) This software package can also be re-distributed under
  *    particular conditions that are _weaker_ than the Perl "Artistic
  *    License" combined with the GNU Library General Public License.
  *    Redistribution need only satisfy all four license notices below.
  *
  * Disclaimer: I am not a lawyer, and neither are the Sunsite Archive
  * Maintainers.
  *
  * END OF SUMMARY INFORMATION
  *
  * ------------------------------------------------------------------
  *
  * License notice 1, covering part of this software package.
  *
  * [Covers the original 1985 afio code]
  *
  * Copyright (c) 1985 Lachman Associates, Inc..
  *
  * This software was written by Mark Brukhartz at Lachman Associates,
  * Inc.. It may be distributed within the following restrictions:
  *      (1) It may not be sold at a profit.
  *      (2) This credit and notice must remain intact.
  * This software may be distributed with other software by a commercial
  * vendor, provided that it is included at no additional charge.
  *
  *
  * [Note: it is believed that condition 5 of the Perl "Artistic
  * License" implies the intent of restriction (1) above.]
  *
  * --------
  *
  * ** License notice 2, covering part of this software package.
  *
  * [Covers the tempnam function]
  *
  * Copyright:   Copyright (c) 1989 by Monty Walls.
  *              Not derived from licensed software.
  *
  *              Permission to copy and/or distribute granted under the
  *              following conditions:
  *
  *              1). This notice must remain intact.
  *              2). The author is not responsible for the consequences of use
  *                      this software, no matter how awful, even if they
  *                      arise from defects in it.
  *              3). Altered version must not be represented as being the
  *                      original software.
  *
  * --------
  *
  * ** License notice 3, covering part of this software package.
  *
  * [Covers the contents of the gnu.fnmatch.tar.gz file]
  *
  *  Copyright (C) 1991, 1992, 1993 Free Software Foundation, Inc.
  *
  *  This library is free software; you can redistribute it and/or
  *  modify it under the terms of the GNU Library General Public License as
  *  published by the Free Software Foundation; either version 2 of the
  *  License, or (at your option) any later version.
  *
  *  This library is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  *  Library General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program; if not, write to the Free Software
  *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
  *
  * --------
  *
  * ** License notice 4, covering part of this software package.
  *
  * [Covers the remainder of this software package]
  *
  * Additional code was written by a large cast of people.
  *
  * All additional code may be redistributed under the conditions of
  * license notice 1.
  *
  * Note that the authors of the additional code retain the right to
  * allow for the re-distribution of their code under weaker (and less
  * exotic) conditions.
  *
  * --------
  *
  * ** WARRANTY NOTICE
  *
  * THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
  * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
  *
  *
  * [End of licensing and redistribution section]
  *
  * ---------------------------------------------------------------------
  *
  * Makefile options:
  *  o Define INDEX to use index() in place of strchr() (v7, BSD).
  *  o Define MEMCPY when an efficient memcpy() exists (SysV).
  *  o Define MKDIR when a mkdir() system call is present (4.2BSD, SysVr3).
  *  o Define NOVOID if your compiler doesn't like void casts.
  *  o Define SYSTIME to use <sys/time.h> rather than <time.h> (4.2BSD).
  *  o Define VOIDFIX to allow pointers to functions returning void (non-PCC).
  *  o Define CTC3B2 to support AT&T 3B2 streaming cartridge tape.
  *  o Define HAVEFCNTL if you have <fcntl.h>
  *  o Define MYTEMPNAM if you don't have tempnam()
  *  o Define UNIXPC if you are on a 3b1, 7300, etc.
  *  o Define HAVEMEMCMP if you have memcmp otherwise assumes bcmp
  *  o Define DEFFMTCMD to being how to format the media you use the most.
  *  o Define LONGZFILE if you want .Z to be tagged on the end of a 14 char
  *
  * BUGS:
  *      See the manpage.
  *
  * Added by Jeff Buhrt:
  *      Floppy Verify/format/restart output in the middle of a set,
  *      compress files on output, extended error messages and logging
  *
  * Added by Dave Gymer:
  *      Lotsa bugfixes, Linux support, recognition of .Z files in an archive
  *      that were compressed already (and shouldn't be uncompressed).
  *      Displays compression ratios.
  *
  * See the HISTORY file for more revision info.  */
